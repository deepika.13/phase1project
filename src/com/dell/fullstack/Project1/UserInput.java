package com.dell.fullstack.Project1;

import java.util.Scanner;



public class UserInput {

	public static void inputMethod(){

		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();

		if(input==1){

			System.out.println("****************Welcome to Registration Page*******************");
			RegistrationPage reg = new RegistrationPage();
			reg.userRegistration();
		}
		else
		{
		System.out.println("****************Welcome to Login Page*******************");
		LoginPage l = new LoginPage();
		l.loginEntry();
		
		}
	}

	public void nameExists(){
		
		System.out.println("---------User-Name already exists , Please try different name-----------");

	}
	
	public void credentialInput(String name){
		CredentialStorage cs = new CredentialStorage();

		System.out.println("****************Welcome to Digital Locker*******************");

		System.out.println("1.Save Credentials");
		System.out.println("2.Retrieve Credentials");
		
		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();

		if(input==1){
			cs.save(name);
			
		}
		else
		{
		
		cs.retrieve(name);
		}
		
		
	}
	
	
public void continueSave(String name){
		
	System.out.println("Do you want to continue Y/N");
	
	Scanner sc = new Scanner(System.in);
	String input = sc.next();

	if(input.equals("Y")){
		System.out.println(name);
		CredentialStorage cs = new CredentialStorage();
		cs.save(name);
		
	}
	else if(input.equals("N"))
	{
	
	System.out.println("THANKYOU..");
	}
		
	}
}
